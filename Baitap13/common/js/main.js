$(document).ready(function() {
	$(".logo a").hover(function() {
		$(".page-logo").addClass("shake animated")
	}, function() {
		$(".page-logo").removeClass("shake animated")
	});

	$(".popular-title").hover(function() {
		$(this).addClass("bounceIn animated")
	}, function() {
		$(this).removeClass("bounceIn animated")
	});

	$(".connect-list").hover(function() {
		$(this).addClass("swing animated")
	}, function() {
		$(this).removeClass("swing animated")
	});

	$(".aside-A a").hover(function() {
		$(".footer-logo").addClass("flipInY animated")
	}, function() {
		$(".footer-logo").removeClass("flipInY animated")
	});

	new WOW().init();
});

var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var ytPlayer = [];
var status = [];
var ytData = [
    {
        id: 'OBT-uJ3XztE', //id video
        area: 'player', //khu vuc bo video
    }, {
        id: 'QV7vgrl0B_M',
        area: 'player-02',
    }, {
    	id: 'lv9OdCwqD1w',
    	area: 'player-03',
    }
];

function onYouTubeIframeAPIReady() {
    for (var i = 0; i < ytData.length; i++) {
        ytPlayer[i] = new YT.Player(ytData[i]['area'], {
            videoId: ytData[i]['id'],
            playerVars: {
                rel: 0,
                playsinline: 1,
            },
            events: {
                'onReady': function(event) {
                    $('.thumb').each(function(i) { //thumb: lop mask de len ytb
                        $(this).on('click', function() {
                            $(this).css({
                                'visibility': 'hidden',
                                'opacity': 0
                            });
                            ytPlayer[i].playVideo();
                        });
                    });
                }
            }
        });
    }
}